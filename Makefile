include $(TOPDIR)/rules.mk

PKG_NAME:=gsmalarm
PKG_RELEASE:=1

PKG_MAINTAINER:=Harald Geyer <harald@ccbib.org>
PKG_LICENSE_FILES:=README.md

PKG_BUILD_DIR:=$(BUILD_DIR)/$(PKG_NAME)/

include $(INCLUDE_DIR)/package.mk

define Package/gsmalarm
  SECTION:=utils
  CATEGORY:=Utilities
  DEPENDS:=smstools3
  TITLE:=Report alarm condiations via GSM network
  PKGARCH:=all
endef

define Build/Prepare
	mkdir -p $(PKG_BUILD_DIR)
	$(CP) ./src/* $(PKG_BUILD_DIR)/
	date >$(PKG_BUILD_DIR)/version
	git show -s --oneline >>$(PKG_BUILD_DIR)/gsmalarm-version
endef

define Build/Configure
endef

define Build/Compile
endef

define Package/gsmalarm/install
	$(INSTALL_DIR) $(1)/etc/gsmalarm/actions
	$(LN) actions $(1)/etc/gsmalarm/commands
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/verify_number_* $(1)/etc/gsmalarm/
	$(LN) verify_number_default $(1)/etc/gsmalarm/verify_number
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/actions/* $(1)/etc/gsmalarm/actions/
	$(INSTALL_DIR) $(1)/usr/lib/gsmalarm/scripts
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/eventhandler $(1)/usr/lib/gsmalarm/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/alarmhandler $(1)/usr/lib/gsmalarm/
	$(INSTALL_DIR) $(1)/usr/bin/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/gsmalarm $(1)/usr/bin/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/smsd_extract_number $(1)/usr/bin/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/smsd_getbody $(1)/usr/bin/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/smsd_process_commands $(1)/usr/bin/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/scripts/* $(1)/usr/lib/gsmalarm/scripts
	$(INSTALL_DATA) $(PKG_BUILD_DIR)/gsmalarm-version $(1)/etc/
	$(INSTALL_CONF) ./files/conf $(1)/etc/gsmalarm/
endef

$(eval $(call BuildPackage,gsmalarm))

