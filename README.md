gsmalarm is a set of scripts on top of
[smstools3](http://smstools3.kekekasvi.com/), to turn an embedded
system with an (USB)-modem into an alarm system. The scripts are intended
to run on OpenWRT but should be useable or easily adaptable for any unix
like system. A Makefile for building an OpenWRT package is included.

A ready built OpenWRT package may be available at:
[http://friends.ccbib.org/lambda/](http://friends.ccbib.org/lambda/)


# Usage of the alarm system

A gsmalarm system can be controlled and queried via SMS. The actual alarms
are delivered via voicecall (the modem has to support this). Upon reception
of an alarm call the user has to deny the call. If the call is accepted,
gsmalarm will assume that it has entered some kind of voicebox and will
hang up immediatly.

To ACK an alarm the user has to call the alarm system back. gsmalarm will
deny the call.

Basic checks are implemented to ensure that only authorized numbers (ie users)
are controlling the alarm system: Multiple numbers can be whitelisted. Each
whitelisted number can send commands via SMS. To actually receive alarm
calls the user has to send a "subscribe" command (see below). Only the most
recently subscribed number will receive alarm calls.


## SMS commands

Commands are processed in the order in which they appear in the text message.
The entire message is converted to lower case before parsing, to make
commands case-insensitive from the POV of the user. Unkown commands (words)
are ignored. Some commands take extra arguments, therefore effectively
eating the following words from the message. The following commands are
available at the moment, but not all of them need to be enabled on any given
system and it is trivial to add new custom commands.

* subscribe - subscribe the sender to alarm calls
* whitelist 'number' - add 'number' to the list of authorized users
* systemstatus - send back a text message containing
  + the currently subscribed number
  + the uptime of the system
  + the full list of whitelisted numbers
* systemhalt, systemreboot - Try to halt or reboot the system


# API

The API towards software packages using the alarm system is fairly simple,
working only by executing two commands:

* Executing 'gsmalarm' will cause an alarm call to be sent
* Upon reception of an ACK call, the script /etc/gsmalarm/ack will be
  executed if available.

In particular it is the responsibility of the other software to retrigger
alarm calls if no ACK call was received, should such behaviour be desired.

For proper integration with smstools3 the eventhandler (and probably the
alarmhandler) need to be added to the smsd configuration.


## Filesystem layout

The following information is meant for the administrator setting up and
maintaining a gsmalarm installation. Most of the files needing the
administrators attention reside below /etc/gsmalarm/:

* /etc/gsmalarm/actions/
  directory containing the scripts implementing control commands. These are
  shipped in the source tree in 'src/actions'.

* /etc/gsmalarm/commands/
  directory containing the actually available control commands on the
  installation. Either by linking the scripts in the 'actions' directory
  or by custom local scripts. Note that control commands can be easily
  supported in local languages by adding or renaming links in this directory.

* /etc/gsmalarm/number
  textfile containing exactly one line, the number currently subscribed to
  alarm calls. Usually updated the the control command.

* /etc/gsmalarm/ack
  executable getting called when ever an ACK comes in. This is to be
  provided by the software using gsmalarm.

* /etc/gsmalarm/whitelist
  textfile listing one whitelisted number per line. Usually updated by the
  control command, but the administrator needs to provide at least one valid
  number for this to work.

* /etc/gsmalarm/verify_number
  executable used by the 'whitelist' command  to check if the supplied
  argument is a valid number. Two example scripts are provided in the source
  tree: 'src/verify_number_default' to check if the argument only contains
  digits and 'src/verify_number_at' to check for a number from a specific
  country.

* /etc/gsmalarm/conf
  text file containing shell code (need not be executeable) to set environment
  variables for configrations. At the moment this is only
  + 'smsd_outgoing' the outgoing directory of the smsd.

The 'gsmalarm' script and the event- and alarmhandler need to be installed
in proper locations for the respective other software packages to find them.

The scripts following the pattern 'src/smsd_*' need to be installed somewhere
in the $PATH of the eventhandler.

There is also an example/stub configuration file for smsd provided:
'e1750.conf'


# Additional scripts for house keeping and reliability

A setup as above is sufficient to make a fully operational gsmalarm system.
However depending on the HW (some embedded systems are not well suited for
long term applications) and the environment (for example EMI in an industrial
setting) reliablilty might be an issue. The scripts located in 'src/scripts/'
were written to address such problems. This scripts very likely need some
editing to fit any specific situation - use with care.

If the alarmhandler fails to detect the modem, it tries to execute a script
/etc/reset-usb. 'reset-usb' should power cycle the usb bus to force
reenumeration of the attached devices. As this cannot be done in general, a
'reset-usb' scripts is not included in the source distribution. However
'reset-usb-olinuxino' is a specific example for use on the imx233-olinuxino
boards.

'reset-reboot100' is example code to add to 'reset-usb' to log resets and
reboot the whole system after 100 attempts to reset the bus as sometimes
resetting the bus doesn't seem to be enough. In the process information that
might be useful for diagnosing the problem is written to '/gsmalarm_reset.log'.

'smsd_check' is a example cron job monitoring the health of smsd processes
and restarting the service if necessary. Ideally smsd would integrate better
with service managers like procd to make this obsolete.


# Hardware

Every modem that supports voice calls (and works with smstools3) should
do. I'm using a Huawai E1750 - however there are devices with and without
voice call support available. The version that supports voice calls typically
registers 3 terminal devices under linux: ttyUSB0, ttyUSB1, ttyUSB2.
(The version without voice call support only registers two of them.) I got
mine from [Olimex](https://www.olimex.com/Products/USB-Modules/MOD-USB3G/).


# Bugs and patches

Of course gmsalarm comes without any warranty. I use it for serious (but not
critical) applications, however this doesn't imply that it will work for you.

If you notice any problems, please tell me at harald@ccbib.org - if you have
any improvements, consider pushing them to the 'mob' branch at
[repo.or.cz](http://repo.or.cz/gsmalarm.git) and drop me an e-mail.


# Copyright and license

The scripts making up this software are trivial enough, that they very likely
aren't subject to copyright law in most jurisdictions. However to avoid
any doubt, I confirm that they have been written by myself, Harald Geyer,
with the occasional inspiration taken from the
[smstools3 forum](http://smstools3.kekekasvi.com/board.php).

I hereby license any parts of this work, that might be subject to copyright
law, to everybody without any conditions.
